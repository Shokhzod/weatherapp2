import UIKit
import Foundation


class loading {
    
    func loadingJson(completion : @escaping (OneDayWeather) -> Void) {
        guard let url = URL(string: OneDayUrl) else { return }
        URLSession.shared.dataTask(with: url) { (data, respons, error) in
            
            do{
                guard let data = data else { return }
                guard  error == nil else { return }
                
                let decoder = JSONDecoder()
                
                let weather = try decoder.decode(OneDayWeather.self, from: data)
                DispatchQueue.main.async {
                    completion(weather)
                } }catch { print(error) }
        } .resume()
    }
    
    func loadingDailyJson(completion : @escaping (DailyWeather)-> Void){
        
        guard let url = URL(string: SevenDayUrl ) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, resonse, error) in
            
            do{
                guard let data = data else { return }
                guard error == nil else { return }
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                
                let weather = try decoder.decode(DailyWeather.self, from: data)
                DispatchQueue.main.async {
                    
                    completion(weather)
                }
                
                
            }catch { print(error) }
            
        }.resume()
        
    }
    
    
}
