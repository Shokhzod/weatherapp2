import Foundation
import Alamofire

class loadingJsonAlamofire{
    
    func json1Alamofire(complation: @escaping (OneDayWeather)-> Void) {
        
        Alamofire.AF.request(OneDayUrl).response { response in
            guard let data = response.data else { return }
            
            do{
                let decoder = JSONDecoder()
                let weather = try decoder.decode(OneDayWeather.self, from: data)
                DispatchQueue.main.async {
                    complation(weather)
                    print(weather)
                }
            }catch{ print("\(error)") }
        }
    }
        
    
    func json7Alamofire(completion: @escaping (DailyWeather)-> Void){
        
        Alamofire.AF.request(SevenDayUrl).response { response in
            guard let data = response.data else { return }
            
            do{
                
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .secondsSince1970
                let weather = try decoder.decode(DailyWeather.self, from: data)
                
                DispatchQueue.main.async {
                    completion(weather)
                    print(weather)
                }
            }catch{ print(error) }
        }
    }
    
    
    
    }
    

