//
//  ViewController.swift
//  WeathersApp
//
//  Created by Shokhzod on 15/04/21.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    var realm : Realm!
    
    @IBOutlet weak var titleMetodLabel: UILabel!
    @IBOutlet weak var tempMetodLabel: UILabel!
    
    @IBOutlet weak var secondTitleLabel: UILabel!
    @IBOutlet weak var secondMetodLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleMetodLabel.text = "Стандартный метод"
        self.secondTitleLabel.text = "Alamofire"
    
    }

    @IBAction func StandartMetodeBTN(_ sender: Any) {
        self.titleMetodLabel.text = "Wait please..."
        DispatchQueue.main.async {
            loading().loadingJson { (weather) in
                self.tempMetodLabel.text = "\(String(format:"%.1f", (weather.main.temp))) ºC"
                self.titleMetodLabel.text = "STANDART METOD DONE!"
            }
        }
    }
    
    @IBAction func AlamofireMetodBTN(_ sender: Any) {
        self.secondTitleLabel.text = "Wait please..."
        DispatchQueue.main.async {
            loadingJsonAlamofire().json1Alamofire { (weather) in
                self.secondMetodLabel.text = "\(String(format: "%.1f", (weather.main.temp))) ºC"
                self.secondTitleLabel.text = "ALAMOFIRE METOD DONE!"
            }
        }
    }
    
    @IBAction func StandarSenderBTN(_ sender: Any) {
        performSegue(withIdentifier: "showSecondVC", sender: "Standart")
    }
    
    @IBAction func AlamofireSernderBTN(_ sender: Any) {
        performSegue(withIdentifier: "showSecondVC", sender: "Alamofire")
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showSecondVC" ,let destiation = segue.destination as? SecondViewController , let sender = sender as? String else { return }
    
        switch sender {
        case "Standart":
            destiation.StandartTable()
        case "Alamofire":
            destiation.AlamofireTable()
        default:
            return
        }
    print("SenderRRRRRR -> \(sender)")
    }
}


