//
//  SecondViewController.swift
//  WeathersApp
//
//  Created by Shokhzod on 15/04/21.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var weather2 : [Temp] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func StandartTable() {
        let jsonCopy = loading()
        jsonCopy.loadingDailyJson { (data) in
            self.weather2 = data.daily
            self.tableView.reloadData()
        }
        
    }
    
    func AlamofireTable() {
        let jsonCopy = loadingJsonAlamofire()
        jsonCopy.json7Alamofire { (data) in
            self.weather2 = data.daily
            self.tableView.reloadData()
        }
    }
    
}

extension SecondViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return weather2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as! TableViewCell
        
        let index = weather2[indexPath.row]
        cell.DayLabel.text = "\(index.dt)"
        cell.TempLabel.text = "\(String(format: "%.1f", (index.temp.day))) ºC"
        
        
        return cell
    }
    

    
}
