import UIKit
import Foundation



// Wetaher for one day
struct OneDayWeather : Decodable{
    let main : TempOneDay
    let name : String
}

struct TempOneDay: Decodable {
    let temp: Double
}


// Waether for 7 days

struct DailyWeather: Decodable {
    let daily: [Temp]
}

struct Temp: Decodable {
    let dt: Date
    let temp: TempDay
}

struct TempDay: Decodable {
    let day: Double
}
