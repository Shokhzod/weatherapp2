//
//  TableViewCell.swift
//  WeathersApp
//
//  Created by Shokhzod on 15/04/21.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var DayLabel: UILabel!
    @IBOutlet weak var TempLabel: UILabel!
    
}
